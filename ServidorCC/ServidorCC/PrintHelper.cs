﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aloha.SDK.Interceptor
{
    public class PrintHelper
    {
        public static string AddTextAtTop2(string originalXml, List<string> textLines)
        {
            string newXml = originalXml;
            try
            {
                string printStyleTag = "</PRINTSTYLE>";
                int pos = newXml.IndexOf(printStyleTag) + printStyleTag.Length;
                if (pos < 0)
                {
                    printStyleTag = "<STARTJOURNAL/>";
                    pos = newXml.IndexOf(printStyleTag) + printStyleTag.Length;
                }
                StringBuilder tmp = new StringBuilder();
                foreach (string textLine in textLines)
                {
                    tmp.Append(String.Format("<PRINTCENTERED>{0}</PRINTCENTERED>", textLine));
                }
                newXml = newXml.Insert(pos, tmp.ToString());
            }
            catch
            {
                return originalXml;
            }
            return newXml;
        }
        public static string AddTextAtFinal(string originalXml, string textLine)
        {
            string newXml = originalXml;
            try
            {
                string printLine = "<STOPJOURNAL/>";
                int pos = newXml.Trim().LastIndexOf(printLine);
                if (pos == -1)
                {
                    printLine = "<STOPJOURNAL />";
                    pos = newXml.LastIndexOf(printLine);
                }
                StringBuilder temp = new StringBuilder();
                temp.Append(String.Format("<PRINTCENTERED>{0}</PRINTCENTERED>", textLine));
                newXml = newXml.Insert(pos, temp.ToString());
            }
            catch
            {
                return originalXml;
            }
            return newXml;
        }
    }
}
