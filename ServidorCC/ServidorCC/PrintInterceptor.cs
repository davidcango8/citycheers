﻿using LasaFOHLib;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Aloha.SDK.Interceptor
{
    [ComVisible(true)]
    [GuidAttribute("FFCE2D23-E82A-48E5-B1FF-4D1E646634B2")]
    public class PrintInterceptor : IInterceptAlohaPrinting2, IInterceptPrintLicense
    {
        private ILog Log;

        public PrintInterceptor()
        {
            Uri XmlConfigurator = new Uri(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Aloha.SDK.Interceptor.dll.config"));
            log4net.Config.XmlConfigurator.Configure(XmlConfigurator);
            Log = log4net.LogManager.GetLogger("PrintInterceptor");
            Log.Info("Starting Intercept Printing Class");
        }

        #region Component Category Registration

        [ComRegisterFunction()]
        static void Reg(String regKey)
        {
            Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(regKey.Substring(18) + "\\Implemented Categories\\" + "{D0579B21-8915-4851-B99F-798F51E2A3BB}");
        }

        [ComUnregisterFunction()]
        static void Unreg(String regKey)
        {
            try
            {
                Microsoft.Win32.Registry.ClassesRoot.DeleteSubKey(regKey.Substring(18) + "\\Implemented Categories\\" + "{D0579B21-8915-4851-B99F-798F51E2A3BB}");
            }
            catch (Exception)
            {
            }

        }

        #endregion
        public string GetLicInfo()
        {
            return @"272V5J2F0X0>3P5;49373X1;4R5Q3225245E4:1Q3T0K5D1N0H2I3N3E1E4I411;15080:5E";
        }

        public string PrintXML(int FOHDocumentType, string xmlIn)
        {
            string xmlOut = xmlIn;
            Log.Info($"Inicia Print XML Interceptor para nueva cuenta tipo: {FOHDocumentType} {(DOCTYPES)FOHDocumentType}");

            List<string> header = new List<string>();
            header.Add("PRUEBA DE ENCABEZADO 1");
            header.Add("PRUEBA DE ENCABEZADO 2");
            header.Add("PRUEBA DE ENCABEZADO 3");
            header.Add("PRUEBA DE ENCABEZADO 4");
            header.Add("PRUEBA DE ENCABEZADO 5");

            xmlOut = PrintHelper.AddTextAtTop2(xmlOut, header);

            xmlOut = PrintHelper.AddTextAtFinal(xmlOut, "FIN DE LA PRUEBA");
            return xmlOut;
        }

        public DOCTYPES[] GetSubscribeOptions()
        {
            DOCTYPES[] types = {   DOCTYPES.FOHDOC_CHECK                  //Cuenta del Cliente
                                   //DOCTYPES.FOHDOC_CHECKOUT,            //Corte del Mesero
                                   //DOCTYPES.FOHDOC_PMIX,                //Reporte de Product Mix 
                                   //DOCTYPES.FOHDOC_RESTAURANT_SALES,    //Reporte de Ventas de Restaurante
                                   //DOCTYPES.FOHDOC_VOUCHER,             //Voucher de Tarjeta de Credito
                                   //DOCTYPES.FOHDOC_CLOCK_OUT            //Impresion de Clock Out
                               };

            return types;
        }

        public NOTIFY_OPTIONS[] GetNotifyOptions()
        {
            NOTIFY_OPTIONS[] notify = { NOTIFY_OPTIONS.NTFY_PRINT, NOTIFY_OPTIONS.NTFY_POSTPRINT };
            return notify;
        }

        public void PrePrint(DOCTYPES DocType, string XmlPrintStream)
        {
            throw new NotImplementedException();
        }

        public void PostPrint(DOCTYPES DocType, string XmlPrintStream)
        {
            Log.Info("XML PRINT Final");
            Log.Info(XmlPrintStream);
        }


    }
}
