﻿using Aloha.SDK.Common;
using Aloha.SDK.Common.Aloha_Files;
using LasaFOHLib;
using log4net;
using System;
using System.IO;
using System.Reflection;

namespace Aloha.SDK.Interceptor
{
    public class ActivityHandler
    {
        private ILog Log;
        private SdkFunctions _sdkFunctions;
        object thisLock = new Object();

        public ActivityHandler()
        {
            Uri XmlConfigurator = new Uri(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Aloha.SDK.Interceptor.dll.config"));
            log4net.Config.XmlConfigurator.Configure(XmlConfigurator);
            Log = log4net.LogManager.GetLogger("ActivityHand");
            Log.Info("Starting Activity Handler Class");
            _sdkFunctions = new SdkFunctions();
        }

        public void LogIn(int EmployeeId, string Name)
        {
            Log.Info($"Empleado Logeado: {EmployeeId} {Name}");
        }

        public void InitializationHandler()
        {
            lock (thisLock)
            {
                try
                {
                    Log.Info("Method InitializationHandler() called.");
                }
                catch
                { }
            }
        }

        public void Bump(int TableId)
        {
            lock (thisLock)
            {
                try
                {
                    if (TableId == 0)
                        return;
                    IIberDepot iberDepot = AlohaSdkFactory.GetIberDepotInstance();

                    IberObject objectTable = iberDepot.FindObjectFromId((int)COMEnums.INTERNAL_TABLES, TableId).First();
                    IberObject objCurrentCheck = objectTable.GetEnum((int)COMEnums.INTERNAL_TABLES_CHECKS).First();

                    Log.Info("************Información de la cuenta Bump ************");
                    Log.Info($"     ID de la mesa: {objectTable.GetLongVal("ID")}");
                    Log.Info($"     NAME de la mesa: {objectTable.GetStringVal("NAME")}");
                    Log.Info($"     ID de la cuenta: {_sdkFunctions.GetCheckNumberFromCheckId(objCurrentCheck.GetLongVal("ID"))}");
                    Log.Info($"     Status de la cuenta: {objCurrentCheck.GetLongVal("DELIVERYSTATUS")}");

                    /*
                        IberObject objCurrentCheckEntry = objCurrentCheck.GetEnum((int)COMEnums.INTERNAL_CHECKS_ENTRIES).First();
                        while (objCurrentCheckEntry != null)
                        {
                            try
                            {
                                int xModCode = objCurrentCheckEntry.GetLongVal("MOD_CODE");
                                if ((objCurrentCheckEntry.GetBoolVal("TYPE") == 0) && !(xModCode == 8 || xModCode == 12 || xModCode == 13))
                                {
                                    int xEntryId = 0;
                                    try
                                    {
                                        xEntryId = objCurrentCheckEntry.GetLongVal("ID");
                                        //Log.Info($"Loading data for EntryId: {xEntryId}");
                                        ItemDefinition item = GetItemFromId(objCurrentCheckEntry.GetLongVal("DATA"));  //DATA is really ItemId 
                                        item.Price = objCurrentCheckEntry.GetDoubleVal("PRICE");
                                        item.Disp_Price = objCurrentCheckEntry.GetStringVal("DISP_PRICE");
                                        itemInfo.Add(item);
                                    }
                                    catch
                                    { }
                                }
                                objCurrentCheckEntry = enumCurrentCheck.Next();
                            }
                        */
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    Log.Error(ex.StackTrace);
                }
            }
        }

        public void FinalBump(int TableId)
        {
            lock (thisLock)
            {
                try
                {
                    if (TableId == 0)
                        return;
                    Log.Info("************Información de la cuenta Bump ************");
                    Log.Info($"     ID de la mesa: {SdkFunctions.GetCheckIdFromCheckNumber(TableId)}");

                    IIberDepot iberDepot = AlohaSdkFactory.GetIberDepotInstance();

                    IberObject objectTable = iberDepot.FindObjectFromId((int)COMEnums.INTERNAL_TABLES, TableId).First();
                    IberObject objCurrentCheck = objectTable.GetEnum((int)COMEnums.INTERNAL_TABLES_CHECKS).First();

                    Log.Info("************Información de la cuenta Bump ************");
                    Log.Info($"     ID de la cuenta: {objCurrentCheck.GetLongVal("ID")}");
                    Log.Info($"     Status de la cuenta: {objCurrentCheck.GetLongVal("DELIVERYSTATUS")}");

                    /*
                        IberObject objCurrentCheckEntry = objCurrentCheck.GetEnum((int)COMEnums.INTERNAL_CHECKS_ENTRIES).First();
                        while (objCurrentCheckEntry != null)
                        {
                            try
                            {
                                int xModCode = objCurrentCheckEntry.GetLongVal("MOD_CODE");
                                if ((objCurrentCheckEntry.GetBoolVal("TYPE") == 0) && !(xModCode == 8 || xModCode == 12 || xModCode == 13))
                                {
                                    int xEntryId = 0;
                                    try
                                    {
                                        xEntryId = objCurrentCheckEntry.GetLongVal("ID");
                                        //Log.Info($"Loading data for EntryId: {xEntryId}");
                                        ItemDefinition item = GetItemFromId(objCurrentCheckEntry.GetLongVal("DATA"));  //DATA is really ItemId 
                                        item.Price = objCurrentCheckEntry.GetDoubleVal("PRICE");
                                        item.Disp_Price = objCurrentCheckEntry.GetStringVal("DISP_PRICE");
                                        itemInfo.Add(item);
                                    }
                                    catch
                                    { }
                                }
                                objCurrentCheckEntry = enumCurrentCheck.Next();
                            }
                        */
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    Log.Error(ex.StackTrace);
                }
            }
        }

        public void AddItemHanler(int EmployeeId, int QueueId, int TableId, int CheckId, int EntryId)
        {
            Log.Debug($"Event AddItemHanler called. Employee Id: {EmployeeId}, CheckId: {CheckId}, EntryId: {EntryId}");
            lock (thisLock)
            {
                try
                {
                    if (CheckId == 0 || EntryId == 0)
                        return;
                    ItemDefinition itemInfo = _sdkFunctions.GetItemInfoFromEntryId(CheckId, EntryId);
                    Log.Info("************Información del Articulo************");
                    Log.Info($"     Nombre Largo: {itemInfo.LongName}");
                    Log.Info($"     Nombre Cocina: {itemInfo.ChitName}");
                    Log.Info($"     Codigo de Exportación: {itemInfo.BOHName}");
                    Log.Info($"     Precio en File: {itemInfo.Price}");
                    Log.Info($"     TaxId Definition: {itemInfo.TaxId}, TaxId Secundario: {itemInfo.TaxId2}");
                    Log.Info($"     LA CUENTA ESTA CERRADA: {_sdkFunctions.IsCheckClose(CheckId).ToString()}");
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    Log.Error(ex.StackTrace);
                    ItemDefinition itemInfo = _sdkFunctions.GetItemFromId(600060); ///Test
                    Log.Info("************Información del Articulo************");
                    Log.Info($"     Nombre Largo: {itemInfo.LongName}");
                    Log.Info($"     Nombre Cocina: {itemInfo.ChitName}");
                    Log.Info($"     Codigo de Exportación: {itemInfo.BOHName}");
                    Log.Info($"     Precio en File: {itemInfo.Price}");
                    Log.Info($"     TaxId Definition: {itemInfo.TaxId}, TaxId Secundario: {itemInfo.TaxId2}");
                }
            }
        }
    }
}