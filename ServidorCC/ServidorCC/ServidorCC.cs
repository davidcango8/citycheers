﻿using Ini.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServidorCC
{
    public partial class ServidorCC : ServiceBase
    {
        IniFile ini;
        public string ip;
        public string puerto;
        public string timeSend;
        public string timeReceive;

        public ServidorCC()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Thread myThread = new Thread(Iniciar);
            myThread.Start();
        }

        public void Iniciar()
        {
            string configPath = @"C:\Users\David Cango\source\repos\citycheersaloha\Settings\bin\Debug\config.ini";

            ini = new IniFile(configPath);
            ip = ini.ReadString("Connection", "IP");
            puerto = ini.ReadString("Connection", "PORT");
            timeSend = ini.ReadString("Connection", "TIME_SEND");
            timeReceive = ini.ReadString("Connection", "TIME_RECEIVE");

            ServerCC s = new ServerCC();

            s.ConexionServidor(ip, puerto, timeSend, timeReceive);
            s.IniciarServidor();
        }

        protected override void OnStop()
        {
            var resultado = from item in System.Diagnostics.Process.GetProcesses()
                            where item.ProcessName.ToUpper() == "ServidorCC"
                            select item;

            foreach (var item in resultado)
            {
                item.Kill();
            }
        }
    }
}
