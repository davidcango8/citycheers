﻿using Aloha.SDK.Common;
using LasaFOHLib;
using log4net;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace Aloha.SDK.Interceptor
{
    [ComVisible(true)]
    [GuidAttribute("0B027D17-7A27-4E2B-B8CB-2001CB3E1724")]
    public class ActivityInterceptor : LasaActivity.IInterceptAlohaActivity, LasaActivity.IActivityLicense
    {
        private ILog Log;
        ActivityHandler ActivityInstance;

        public ActivityInterceptor()
        {
            Uri XmlConfigurator = new Uri(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Aloha.SDK.Interceptor.dll.config"));
            log4net.Config.XmlConfigurator.Configure(XmlConfigurator);
            Log = log4net.LogManager.GetLogger("ActivityInterceptor");
            Log.Info("Starting Activity Intercept Class");
            ActivityInstance = new ActivityHandler();
        }

        #region Component Category Registration
        [ComRegisterFunction()]
        static void Reg(String regKey)
        {
            Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(regKey.Substring(18) + "\\Implemented Categories\\" + "{9A2AD147-C505-45ec-9383-666A3487A3A0}"); //InterceptActivity
        }

        [ComUnregisterFunction()]
        static void Unreg(String regKey)
        {
            try
            {
                Microsoft.Win32.Registry.ClassesRoot.DeleteSubKey(regKey.Substring(18) + "\\Implemented Categories\\" + "{9A2AD147-C505-45ec-9383-666A3487A3A0}");
            }
            catch
            {
            }
        }
        #endregion

        public void LogIn(int EmployeeId, string Name)
        {
            Log.Info($"Empleado Logeado: {EmployeeId} {Name}");
        }

        public void LogOut(int EmployeeId, string Name)
        {

        }

        public void ClockIn(int EmployeeId, string EmpName, int JobcodeId, string JobName)
        {

        }

        public void ClockOut(int EmployeeId, string EmpName)
        {

        }

        public void OpenTable(int EmployeeId, int QueueId, int TableId, int TableDefId, string Name)
        {

        }

        public void CloseTable(int EmployeeId, int QueueId, int TableId)
        {

        }

        public void OpenCheck(int EmployeeId, int QueueId, int TableId, int CheckId)
        {

        }

        public void CloseCheck(int EmployeeId, int QueueId, int TableId, int CheckId)
        {

        }

        public void TransferTable(int FromEmployeeId, int ToEmployeeId, int TableId, string NewName, int IsGetCheck)
        {

        }

        public void AcceptTable(int EmployeeId, int FromTableId, int ToTableId)
        {

        }

        public void SaveTab(int EmployeeId, int TableId, string Name)
        {

        }

        public void AddTab(int EmployeeId, int FromTableId, int ToTableId)
        {

        }

        public void NameOrder(int EmployeeId, int QueueId, int TableId, string Name)
        {

        }

        public void Bump(int TableId)
        {
            ThreadStart ts = () => { ActivityInstance.Bump(TableId); };
            Thread newThread = new Thread(ts);
            newThread.Start();
        }

        public void FinalBump(int TableId)
        {
            ThreadStart ts = () => { ActivityInstance.Bump(TableId); };
            Thread newThread = new Thread(ts);
            newThread.Start();
        }
        public void AddItem(int EmployeeId, int QueueId, int TableId, int CheckId, int EntryId)
        {
            ThreadStart ts = () => { ActivityInstance.AddItemHanler(EmployeeId, QueueId, TableId, CheckId, EntryId); };
            Thread newThread = new Thread(ts);
            newThread.Start();
        }

        public void ModifyItem(int EmployeeId, int QueueId, int TableId, int CheckId, int EntryId)
        {

        }

        public void OrderItems(int EmployeeId, int QueueId, int TableId, int CheckId, int ModeId)
        {

        }

        public void HoldItems(int EmployeeId, int QueueId, int TableId, int CheckId)
        {

        }

        public void OpenItem(int EmployeeId, int EntryId, int ItemId, string Description, double Price)
        {

        }

        public void SpecialMessage(int EmployeeId, int MessageId, string Message)
        {

        }

        public void DeleteItems(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int ReasonId)
        {

        }

        public void UpdateItems(int EmployeeId, int QueueId, int TableId, int CheckId)
        {

        }

        public void ApplyPayment(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int TenderId, int PaymentId)
        {

        }

        public void AdjustPayment(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int TenderId, int PaymentId)
        {

        }

        public void DeletePayment(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int TenderId, int PaymentId)
        {

        }

        public void ApplyComp(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int CompTypeId, int CompId)
        {

        }

        public void DeleteComp(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int CompTypeId, int CompId)
        {

        }

        public void ApplyPromo(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int PromotionId, int PromoId)
        {

        }

        public void DeletePromo(int ManagerId, int EmployeeId, int QueueId, int TableId, int CheckId, int PromotionId, int PromoId)
        {

        }

        public void Custom(string Name)
        {
            IIberFuncs23 iberFunction = AlohaSdkFactory.GetIberFuncs23Instance();
            switch (Name)
            {
                case "Aloha.SDK.Demo.CustomTest1":
                    iberFunction.DisplayMessage("Mensaje de Prueba");
                    break;

                default:
                    Log.Info($"Custom Event Ignored: {Name}");
                    break;
            }
        }

        public void Startup(int hMainWnd)
        {

        }

        public void InitializationComplete()
        {
            ThreadStart ts = () => { ActivityInstance.InitializationHandler(); };
            Thread newThread = new Thread(ts);
            newThread.Start();
        }

        public void Shutdown()
        {

        }

        public void CarryoverId(int Type, int OldId, int NewId)
        {

        }

        public void EndOfDay(int IsMaster)
        {

        }

        public void CombineOrder(int EmployeeId, int SrcQueueId, int SrcTableId, int SrcCheckId, int DstQueueId, int DstTableId, int DstCheckId)
        {

        }

        public void OnClockTick()
        {

        }

        public void PreModifyItem(int EmployeeId, int QueueId, int TableId, int CheckId, int EntryId)
        {

        }

        public void LockOrder(int TableId)
        {

        }

        public void UnlockOrder(int TableId)
        {

        }

        public void SetMasterTerminal(int TerminalId)
        {

        }

        public void SetQuickComboLevel(int EmployeeId, int QueueId, int TableId, int CheckId, int PromotionId, int PromoId, int nLevel, int nContext)
        {

        }

        public string GetLicInfo()
        {
            return @"432M342Y4E4H2Q5W4C582T3:1D5F2J5?5H0A530K295E4Z1L0W111>5V3A503E074R3F373Z";
        }
    }
}