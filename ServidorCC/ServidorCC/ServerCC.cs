﻿using Aloha.SDK.Common;
using Ini.Net;
using LasaFOHLib;
using Newtonsoft.Json;
using ServidorCC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServidorCC
{
    class ServerCC
    {
        Socket socketServidor;
        Socket socketCliente;
        private EnvironmenVariablesHelper _environmenVariablesHelper = new EnvironmenVariablesHelper();
        private SdkFunctions _sdkFunctions = new SdkFunctions();

        public void ConexionServidor(string ip, string puerto, string timeSend, string timeReceive)
        {
            try
            {
                socketServidor = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socketServidor.ReceiveTimeout = Convert.ToInt32(timeSend) * 1000;
                socketServidor.SendTimeout = Convert.ToInt32(timeReceive) * 1000;

                IPEndPoint miDireccion = new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(puerto));

                socketServidor.Bind(miDireccion); // Asociamos el socket a miDireccion
                socketServidor.Listen(100); // Lo ponemos a escuchar

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void IniciarServidor()
        {
            Thread conexiones;
            while (true)
            {
                try
                {
                    Console.WriteLine("Esperando Conexiones");
                    socketCliente = socketServidor.Accept();
                    conexiones = new Thread(clientRecibirDatos);
                    conexiones.Start(socketCliente);
                    Console.WriteLine("Se ha conectado un cliente");
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
        }


        public void clientRecibirDatos(object s)
        {

            Socket s_Client = (Socket)s;
            byte[] buffer;
            string message;
            int endIndex;


            buffer = new byte[1024];
            try
            {
                s_Client.Receive(buffer);
            }
            catch (Exception ex)
            {
                byte[] response = Encoding.ASCII.GetBytes(ex.Message);
                s_Client.Send(response);
            }
            message = Encoding.ASCII.GetString(buffer);
            endIndex = message.IndexOf('\0');

            if (endIndex >= 0)
            {
                message = message.Substring(0, endIndex);
            }
            Console.WriteLine("Mensajer Recibido: " + message);

            try
            {
                Data data = new Data();
                data = JsonConvert.DeserializeObject<Data>(message);
                string function = data.function;

                if (function == "addtable")
                {
                    int TermId = data.TermId;
                    int QueueId = data.QueueId;
                    int TableNum = data.TableNum;
                    string TableName = data.TableName;
                    int NumGuests = data.NumGuests;

                    try
                    {
                        IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();

                        int tableId = xFunction.AddTable(TermId, QueueId, TableNum, TableName, NumGuests);
                        int checkId = xFunction.AddCheck(TermId, tableId);
                        _sdkFunctions.RefreshPOSScreen();

                        byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK\",\n    TableId: " + tableId + ",\n    CheckId: " + checkId + ",\n}");
                        s_Client.Send(response);
                    }
                    catch (Exception ex)
                    {
                        byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: " + "\"" + ex.Message + "\"" + "\n}");
                        s_Client.Send(response);
                    }
                }
                if (function == "addcheck")
                {
                    int TermId = data.TermId;
                    int TableId = data.TableId;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.AddCheck(TermId, TableId);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK ADDCHECK\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "enteritem")
                {
                    int CheckId = data.CheckId;
                    int ItemNum = data.ItemNum;
                    double Price = data.Price;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.EnterItem(CheckId, ItemNum, Price);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK ENTERITEM\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "orderitems")
                {
                    int TermId = data.TermId;
                    int TableId = data.TableId;
                    int OrderModeId = data.OrderModeId;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.OrderItems(TermId, TableId, OrderModeId);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK ORDERITEMS\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "applypayment")
                {
                    int TermId = data.TermId;
                    int CheckId = data.CheckId;
                    int TenderId = data.TenderId;
                    double amount = data.amount;
                    double Tip = data.Tip;
                    string CardId = data.CardId;
                    string ExpDate = data.ExpDate;
                    string TrackInfo = data.TrackInfo;
                    string AuthCode = data.AuthCode;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.ApplyPayment(TermId, CheckId, TenderId, amount, Tip, CardId, ExpDate, TrackInfo, AuthCode);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK APPLYPAYMENT\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "applycomp")
                {
                    int TermId = data.TermId;
                    int ManagerId = data.ManagerId;
                    int CheckId = data.CheckId;
                    int CompTypeId = data.CompTypeId;
                    double dAmount = data.dAmount;
                    string Unit = data.Unit;
                    string Name = data.Name;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.ApplyComp(TermId, ManagerId, CheckId, CompTypeId, dAmount, Unit, Name);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK APPLYDISCOUNT\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "closecheck")
                {
                    int TermId = data.TermId;
                    int CheckId = data.CheckId;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.CloseCheck(TermId, CheckId);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK CLOSECHECK\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "closetable")
                {
                    int TermId = data.TermId;
                    int TableId = data.TableId;
                    IIberFuncs23 xFunction = AlohaSdkFactory.GetIberFuncs23Instance();
                    xFunction.CloseTable(TermId, TableId);
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"OK CLOSETABLE\"" + "\n}");
                    s_Client.Send(response);
                }
                if (function == "")
                {
                    byte[] response = Encoding.ASCII.GetBytes("{ \n" + "    message: \"Empty Function\"" + "\n}");
                    s_Client.Send(response);
                }
            }
            catch (Exception ex)
            {
                byte[] response = Encoding.ASCII.GetBytes(ex.Message);
                s_Client.Send(response);
            }

        }
    }
}
