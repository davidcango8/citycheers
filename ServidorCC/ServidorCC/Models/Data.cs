﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServidorCC.Models
{
    class Data
    {
        public string function { get; set; }
        public int TermId { get; set; }
        public int TableId { get; set; }
        public int CheckId { get; set; }
        public int ItemNum { get; set; }
        public double Price { get; set; }
        public int OrderModeId { get; set; }
        public int TenderId { get; set; }
        public double amount { get; set; }
        public double Tip { get; set; }
        public string CardId { get; set; }
        public string ExpDate { get; set; }
        public string TrackInfo { get; set; }
        public string AuthCode { get; set; }
        public int ManagerId { get; set; }
        public int CompTypeId { get; set; }
        public double dAmount { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public int QueueId { get; set; }
        public int TableNum { get; set; }
        public string TableName { get; set; }
        public int NumGuests { get; set; }
        public string message { get; set; }
    }
}
