﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestModule.Controls;

namespace TestModule
{
    public partial class Main : Form
    {
        ClienteSocket socket = new ClienteSocket();

        public string mensajeEnviado;
        public string mensajeRecibido;
        public static string ip;
        public static string puerto;
        public static int timeSend;
        public static int timeReceive;

        public static string recibidoServidor;

        public Main()
        {
            InitializeComponent();
        }

        private void btnSRM_Click(object sender, EventArgs e)
        {
            btnSRM.Enabled = false;
            string sMensaje = "The following errors were detected:\n\n";
            bool bError = false;

            if (string.IsNullOrWhiteSpace(txtIP.Text))
            {
                sMensaje += "Empty IP address\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtPuerto.Text))
            {
                sMensaje += "Empty Port\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtTimeReceive.Text))
            {
                sMensaje += "Empty TimeReceive\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtTimeSend.Text))
            {
                sMensaje += "Empty TimeSend\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtSend.Text))
            {
                sMensaje += "Empty Message\n";
                bError = true;
            }
            if (bError)
            {
                MessageBox.Show(sMensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //config.ReadINI();
                ip = txtIP.Text;
                puerto = txtPuerto.Text;
                timeSend = Convert.ToInt32(txtTimeSend.Text);
                timeReceive = Convert.ToInt32(txtTimeReceive.Text);
                mensajeEnviado = txtSend.Text;
                EnviarMensaje();
            }
            btnSRM.Enabled = true;
        }

        public async void EnviarMensaje()
        {
            var mensajeConexion = socket.Conectar(ip, puerto, timeSend, timeReceive);

            if (mensajeConexion == "Sin conexion con el servidor")
            {
                MessageBox.Show("Could not connect to Server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (mensajeConexion == "Conexion con el servidor")
            {
                try
                {
                    await socket.Enviar(Convert.ToString(mensajeEnviado));
                    recibidoServidor = await socket.Recibir();
                    socket.Desconectar();

                    if (recibidoServidor != null)
                    {
                        txtReceive.Text = recibidoServidor;

                    }
                    else
                    {
                        txtReceive.Text = "null";
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void txtTimeSend_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtTimeSend.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTimeSend.Text = txtTimeSend.Text.Remove(txtTimeSend.Text.Length - 1);
            }
        }

        private void txtTimeReceive_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtTimeReceive.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTimeReceive.Text = txtTimeReceive.Text.Remove(txtTimeReceive.Text.Length - 1);
            }
        }

        private void txtIP_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtIP.Text, "[^0-9.:]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIP.Text = txtIP.Text.Remove(txtIP.Text.Length - 1);
            }
        }

        private void txtPuerto_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtPuerto.Text, "[^0-9.:]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPuerto.Text = txtPuerto.Text.Remove(txtPuerto.Text.Length - 1);
            }
        }
    }
}
