﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TestModule.Controls
{
    class ClienteSocket
    {
        Socket socketCliente;
        public string mensajeError = "Sin conexion con el servidor";
        public string mensajeOk = "Conexion con el servidor";

        public string Conectar(string ip, string puerto, int timeSend, int timeReceive)
        {
            socketCliente = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socketCliente.SendTimeout = timeSend * 1000;
            socketCliente.ReceiveTimeout = timeReceive * 1000;

            try
            {
                IPEndPoint conexionaServidor = new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(puerto));
                socketCliente.Connect(conexionaServidor); // Conectamos  
                Console.WriteLine("Conectado con exito");
                return mensajeOk;
            }

            catch (Exception)
            {
                return mensajeError;
            }
        }

        public void Desconectar()
        {
            socketCliente.Close();
        }

        public async Task Enviar(string datos)
        {
            await Task.Run(() =>
            {
                try
                {
                    byte[] byteDatos = Encoding.ASCII.GetBytes(datos);
                    socketCliente.Send(byteDatos);
                    Console.WriteLine("Enviado");
                }
                catch (Exception)
                {
                }
            });
        }

        public async Task<string> Recibir()
        {
            byte[] buffer = new byte[1024];
            await Task.Run(() =>
            {
                socketCliente.Receive(buffer);
            });
            return byteTostring(buffer);
        }

        public string byteTostring(byte[] buffer)
        {
            string message;
            int endIndex;
            message = Encoding.ASCII.GetString(buffer);
            endIndex = message.IndexOf('\0');
            if (endIndex > 0)
                message = message.Substring(0, endIndex);
            return message;
        }
    }
}
