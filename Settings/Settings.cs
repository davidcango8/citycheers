﻿using Ini.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Settings
{
    public partial class Settings : Form
    {
        private static IniFile ini;
        public Settings()
        {
            InitializeComponent();
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            string sMensaje = "The following errors were detected:\n\n";
            bool bError = false;

            if (string.IsNullOrWhiteSpace(txtIP.Text))
            {
                sMensaje += "Empty IP address\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtPuerto.Text))
            {
                sMensaje += "Empty Port\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtReceive.Text))
            {
                sMensaje += "Empty TimeReceive\n";
                bError = true;
            }
            if (string.IsNullOrWhiteSpace(txtSend.Text))
            {
                sMensaje += "Empty TimeSend\n";
                bError = true;
            }
            if (bError)
            {
                MessageBox.Show(sMensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string ip = txtIP.Text;
                string puerto = txtPuerto.Text;
                string timeSend = txtSend.Text;
                string timeReceive = txtReceive.Text;


                string current_path = Path.GetDirectoryName(Application.ExecutablePath);
                string configPath = current_path + @"\config.ini";
                ini = new IniFile(configPath);
                DialogResult result = MessageBox.Show("Save Changes and RESTART Server?", "Confirmation", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    ini.WriteString("Connection", "IP", ip);
                    ini.WriteString("Connection", "PORT", puerto);
                    ini.WriteString("Connection", "TIME_SEND", timeSend);
                    ini.WriteString("Connection", "TIME_RECEIVE", timeReceive);

                    try
                    {
                        ServiceController service = new ServiceController();
                        service.ServiceName = "citycheersaloha";
                        Console.WriteLine("The service status is currently set to {0}",
                           service.Status.ToString());

                        if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                        {
                            // Start the service if the current status is stopped.
                            Console.WriteLine("Service start...");
                            // Start the service, and wait until its status is "Running".
                            service.Start();
                            service.WaitForStatus(ServiceControllerStatus.Running);

                            // Display the current service status.
                            Console.WriteLine("The service status is currently set to {0}.",
                                               service.Status.ToString());

                            MessageBox.Show("Successful", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        else if ((service.Status.Equals(ServiceControllerStatus.Running)) || (service.Status.Equals(ServiceControllerStatus.StartPending)))
                        {
                            service.Stop();
                            service.WaitForStatus(ServiceControllerStatus.Stopped);
                            service.Start();
                            service.WaitForStatus(ServiceControllerStatus.Running);

                            MessageBox.Show("Successful", " Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (result == DialogResult.No)
                {
                }
                else
                {
                }
            }
        }

        private void txtIP_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtIP.Text, "[^0-9.:]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIP.Text = txtIP.Text.Remove(txtIP.Text.Length - 1);
            }
        }

        private void txtPuerto_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtPuerto.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPuerto.Text = txtPuerto.Text.Remove(txtPuerto.Text.Length - 1);
            }
        }

        private void txtSend_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtSend.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSend.Text = txtSend.Text.Remove(txtSend.Text.Length - 1);
            }
        }

        private void txtReceive_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtReceive.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtReceive.Text = txtReceive.Text.Remove(txtReceive.Text.Length - 1);
            }
        }
    }
}
